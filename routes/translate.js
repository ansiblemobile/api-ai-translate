var express = require('express');
var router = express.Router();
const fs = require('fs');
const translate = require('google-translate-api');

//var path = require('path');

/* GET users listing. */
router.get('/', function(req, res, next) {

    console.log('send');
    var entitiesDir = './public/apiai/Zimabot/entities/'
    var intentDir = './public/apiai/Zimabot/intents/'
    var translationDir = './public/translation/'

    function gtranslate(phrase, lang, textnode) {
        return translate(phrase, {to: lang}).then(res => {
            console.log(res.text);
            textnode = res.text;
            //=> I speak English
            console.log(res.from.language.iso);
            //return  res.text
            //=> nl
        }).catch(err => {
            console.error(err);
        });
    }

    fs.readdir(intentDir, function (err, files) { if (err) throw err;

        // This will only run one file
        var fileindex = 0;
        var maxfile = 100;

        files.forEach( function (file) {
            //myfiles.push(file);
            console.log(file);
            fileindex++

            if(fileindex < maxfile) {

                // Array of all the promises to translate the user says items
                var userSaysTranslationPromises = [];
                var intentFileDir = intentDir + file
                var intentJSON = JSON.parse(require('fs').readFileSync(intentFileDir, 'utf8'));

                // Matches a node in the JSON file to a script and replaces the text
                function matchSpeach(original, replacement) {
                    for (var i = 0; i < intentJSON.responses[0].messages[0].speech.length; i++) {
                        if(intentJSON.responses[0].messages[0].speech == original)
                        {
                            console.log("we havea match Match !")
                            intentJSON.responses[0].messages[0].speech = replacement;
                        }
                    }
                }

                function matchUtterence(original, replacement) {
                    for (var i = 0; i < intentJSON.userSays.length; i++) {
                        console.log("res.original",res.original);
                        console.log("intentJSON.userSays[i].data[0].text",intentJSON.userSays[i].data[0].text);
                        if(intentJSON.userSays[i].data[0].text == original)
                        {
                            console.log("A Match !")
                            intentJSON.userSays[i].data[0].text = replacement;
                        }
                    }
                }


                var translated = [];

                // Loop through all the user says items
                for (var i = 0; i < intentJSON.userSays.length; i++) {

                    var theysaid = intentJSON.userSays[i].data[0].text;
                    console.log("theysaid!!!",theysaid);

                    // Returns a promise that this word will be translated
                    var translatePromeise = translate(theysaid, {from: 'en', to: 'nl'})
                        .then(res => {
                            matchUtterence(res.original, res.text)
                        }).catch(err => {
                            console.error(err);
                        });

                    // Add all these promises to an array
                    translated.push(translatePromeise);
                }

                var speaches = intentJSON.responses[0].messages[0].speech

                // If speaches is an array ( more than one response )
                if(speaches.prop && speaches.prop.constructor === Array)
                {
                    // Loop through resps
                    for (var i = 0; i < speaches.length; i++) {
                        var speach = speaches[i]
                        console.log("speach!!!",speach);
                        var speachPromise = translate(speach, {from: 'en', to: 'nl'})
                            .then(res => {
                                console.log(res.text);
                                matchSpeach(res.original, res.text)
                            })
                            .catch(err => {
                                console.error(err);
                            });

                        // Add all these promises to an array
                        translated.push(speachPromise);

                    } // end for
                }
                else
                {
                    var speachPromise = translate(speaches, {from: 'en', to: 'nl'})
                        .then(res => {
                            console.log(res.text);
                            // In this case it is  a string so just replace with the translation
                            intentJSON.responses[0].messages[0].speech = res.text
                        })
                        .catch(err => {
                            console.error(err);
                        });
                    translated.push(speachPromise);
                }

                // Fires when all the lines of text are fulfilled
                Promise.all(translated).then(function() {
                    console.log("all the files were  parsed created");

                    var intentJSONString = JSON.stringify(intentJSON)
                    var fileDir = translationDir+file
                    console.log("fileDir",fileDir)
                    fs.writeFile(fileDir, intentJSONString, function(err) {
                        if(err) {
                            return console.log(err);
                        }
                        else
                        {
                            console.log("file saved");
                        }
                    });
                });

            }
        });
        res.send('respond with a resource');
    });


    /*
     fs.readdir('./public/apiai/Zimabot', function (err, files) { if (err) throw err;
     files.forEach( function (file) {
     //myfiles.push(file);
     console.log(file);
     });
     res.send('respond with a resource');
     });
     */

    //console.log(myfiles);

});

module.exports = router;
